class FollowController < ApplicationController

  def create

    @follow = Follow.new
    @follow.follower = User.find(session[:id_user])
    @follow.following = User.find(params[:following])

    if @follow.save
      redirect_to show_profile_user_path
    else
      redirect_to users_path
    end
  end
  def destroy
    @follow = Follow.find_by_id(params[:id])
    @follow.destroy
    redirect_to show_profile_user_path
  end
end

# README

# Cuicuitter

Cette application est un Twitter-like nommé Cuicuitter en référence à la mascotte "Cuicuit".

## Mise en place

Les prérequis sont les suivants :

- Ruby 2.7
- Rails 5.2.4.1
- MySQL 

Récupération du projet dans le dossier souhaité 

```bash
git clone https://gitlab.com/Aymard.H/railscuicuitter.git
```

Accès au dossier créé 

```bash
cd railscuicuitter
```

Installation des dépendences

```bash
bundle install
```

Création de la base de données et des tables

```bash
bin/rails db:setup
```

Lancement du serveur 

```bash
bin/rails server
```
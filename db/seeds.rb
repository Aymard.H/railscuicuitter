# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user_list = [
    "Falcon Desbourg",
    "Percila Stellarius",
    "Jasmine Afkar",
    "Ada Lovelace",
    "Andrew Brasgon"
]

user_list.each do |user|
  User.create( name: user)
end

Comment.create(body:"Mon premier Cuicuit",user_id: 1)
Comment.create(body:"Mon second Cuicuit",user_id: 1)
Comment.create(body:"Mon premier Cuicuit P.",user_id: 2)
Comment.create(body:"Mon premier Cuicuit et pas le dernier AG.",user_id: 5)
Comment.create(body:"Mon troisième Cuicuit",user_id: 1)
Comment.create(body:"My first Cuicuit",user_id: 3)
Comment.create(body:"Mon premier Cuicuit A.L.",user_id: 4)
Comment.create(body:"Mon second Cuicuit P.",user_id: 2)
Comment.create(body:"Mon second Cuicuit AG.",user_id: 5)

Follow.create(follower: User.find(1),following: User.find(3))
Follow.create(follower: User.find(1),following: User.find(2))
Follow.create(follower: User.find(5),following: User.find(2))
Follow.create(follower: User.find(5),following: User.find(1))
Follow.create(follower: User.find(2),following: User.find(1))

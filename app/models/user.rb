class User < ApplicationRecord
    has_many :comments, dependent: :destroy
    has_many :followers, :class_name => 'Follow', :foreign_key => 'follower'
    has_many :following, :class_name => 'Follow', :foreign_key => 'following'
    validates :name, presence: true,
                    length: { minimum: 5 }
end
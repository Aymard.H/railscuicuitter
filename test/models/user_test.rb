require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "create_user" do
    users.each do |fixuser|

      user1 = User.new(name:fixuser.name)
      assert_not_nil(user1)

      if fixuser.name.length < 5
        assert(user1.invalid?,user1.name)
      else
        assert(user1.valid?,user1.name)
      end
    end
  end

  test "edit_user" do
      user1 = User.new(name:users(:one).name)
      assert_equal("Bonjour",user1.name)
      user1.update(name:users(:two).name)
      assert_equal(users(:two).name,user1.name)
      user1.update(name:users(:four).name)
      assert_equal(users(:four).name,user1.name)
  end

  test "delete_user" do
    user_test = User.new(name:users(:five).name)
    assert_not_nil(user_test)
  end
end

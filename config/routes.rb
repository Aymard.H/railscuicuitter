Rails.application.routes.draw do
  get 'welcome/index'

  resources :users do
    resources :comments
  end
   
  root 'users#index'

  get 'users/:id/connect', to:'users#connect', as: :connect_user

  get 'follow/:following', to:'follow#create', as: :follow_user

  get 'follow/destroy/:id', to:'follow#destroy', as: :follow_destroy

  get 'profile', to:'users#profile', as: :show_profile_user

end
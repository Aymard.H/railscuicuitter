class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @users = User.all
    find_followers
  end

  def profile
    @user = User.find(session[:id_user])
    find_followers
    @cuicuitline = Comment.where(user_id: @id_follow).order("updated_at", :desc)
  end
      
  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    
    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])
      
    if @user.update(user_params)
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
   
    redirect_to users_path
  end

  def connect
    session[:id_user] = params[:id]
    @user = User.find(params[:id])
    redirect_to show_profile_user_path
  end
  
  private
    def user_params
      params.require(:user).permit(:name)
    end
  private
    def find_followers
      @follows = Follow.where(follower_id: session[:id_user])
      @id_follow = Array.new
      @id_follow.push(session[:id_user])
      @follows.each do |cefollow|
        @id_follow.push(cefollow.following.id)
      end
    end
end

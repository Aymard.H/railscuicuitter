require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "create_follow" do
    user1 = User.new(name: "User 1")
    user2 = User.new(name: "User 2")
    user3 = User.new(name: "User 3")
    follows.each do |fixfollow|
      follow1 = Follow.new(follower: User.find_by_id(fixfollow.follower),following: User.find_by_id(fixfollow.following))
      assert(follow1)
    end
  end

  test "destroy_follow" do
    user1 = User.new(name: "User 1")
    user2 = User.new(name:" User 2")
    user3 = User.new(name:" User 3")
    follows.each do |fixfollow|
      Follow.new(follower: User.find_by_id(fixfollow.follower),following: User.find_by_id(fixfollow.following))
    end
    @follow1 = Follow.first
    @id_follow = @follow1.id
    Follow.destroy(@id_follow)
    assert_raises(ActiveRecord::RecordNotFound) {Follow.find(@id_follow)}
  end
end
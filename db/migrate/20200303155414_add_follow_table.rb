class AddFollowTable < ActiveRecord::Migration[5.2]
  def change
    create_table :follows do |t|
      t.index :follower
      t.index :following
    end
  end
end

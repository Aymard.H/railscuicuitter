class ChangeNameSchemaFollows < ActiveRecord::Migration[5.2]
  def change
    drop_table(:follows)
    create_table :follows do |t|
      t.integer :follower
      t.integer :following
    end
  end
end
